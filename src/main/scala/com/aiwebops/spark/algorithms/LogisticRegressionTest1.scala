package com.aiwebops.spark.algorithms

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.DoubleType

object LogisticRegressionTest1 {

  def main(args:Array[String]):Unit={
    //实例化SparkSession对象
    val spark=SparkSession.builder().appName("Logistic_Prediction").master("local").getOrCreate()
    //设置日志级别，减少日志输出，便于查看运行结果
    spark.sparkContext.setLogLevel("WARN")
    //导入隐式转换包，方便使用转换函数
    import spark.implicits._
    //读取数据，传入数据路径/opt/train/bank_marketing_data.csv
    val bank_Marketing_Data=spark.read
      .option("header", true)
      .option("inferSchema", "true")
      .csv("dataset/bank_marketing_data.csv")
    //查看营销数据的前5条记录，包括所有字段
    println("all columns data:")
    bank_Marketing_Data.show(5)
    //读取营销数据指定的11个字段，并将age、duration、previous三个字段的类型从Integer类型转换为Double类型
    val selected_Data=bank_Marketing_Data.select("age",
      "job",
      "marital",
      "default",
      "housing",
      "loan",
      "duration",
      "previous",
      "poutcome",
      "empvarrate",
      "y")
      .withColumn("age", bank_Marketing_Data("age").cast(DoubleType))
      .withColumn("duration", bank_Marketing_Data("duration").cast(DoubleType))
      .withColumn("previous", bank_Marketing_Data("previous").cast(DoubleType))
    //显示前5条记录，只包含指定的11个字段
    println("11 columns data:")
    selected_Data.show(5)
    //显示营销数据的数据量
    println("data count:"+selected_Data.count())

    //对数据进行概要统计
    val summary=selected_Data.describe()
    println("Summary Statistics:")
    //显示概要统计信息
    summary.show()

    //查看每一列所包含的不同值数量
    val columnNames=selected_Data.columns
    val uniqueValues_PerField=columnNames.map { field => field+":"+selected_Data.select(field).distinct().count() }
    println("Unique Values For each Field:")
    uniqueValues_PerField.foreach(println)







  }

}
